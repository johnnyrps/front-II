# Aula 01 da disciplina Front II do curso de formação Full Stack - Certified Tech Developer


### Resolução da atividade da aula:
<br/>

- No arquivo index.html na linha 10, o link para o arquivo CSS faltava ter a extensão do arquivo .css
- No arquivo main.js na linha 08 na captura do dado informado pelo usuário tinha uma vírgula ao invés de ponto
- No arquivo index.html na linha 83, o link para o arquivo JS faltava adentrar a pasta assets no nível anterior

<br/>

#### Integrantes:
<br/>

- Hugo Almeida
- Pedro Oliveira
- Ruggiero Stello
- Victória Dias