var nomeUsuario = prompt('Informe seu nome completo:');

if(nomeUsuario == '' || nomeUsuario == null) {
    alert('É obrigatório informar o nome!');
    nomeUsuario = prompt('Informe seu nome completo:');
}

var telefoneUsuario = prompt('Informe seu número de telefone:');

if(telefoneUsuario == '' || telefoneUsuario == null) {
    alert('É obrigatório informar o número de telefone!');
    telefoneUsuario = prompt('Informe seu número de telefone:');
}

var ehWhatsapp = confirm("Este número é de Whatsapp?");

alert(`Muito obrigado ${nomeUsuario}, um de nosssos consultores entrará em contato contigo através do número ${telefoneUsuario}`)

console.log(`nome: ${nomeUsuario}, telefone: ${telefoneUsuario}, aceita receber mensagem por Whatsapp: ${ehWhatsapp}`)