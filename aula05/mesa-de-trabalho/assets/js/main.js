var section = document.getElementById('carrinho');

var ul = document.createElement('ul');

ul.classList.add('lista');

section.appendChild(ul);

var texto = ['Carro','Bike','Patinete','Celular','Carregador'];

for(let i = 0; i < 5; i++) {
    var li = document.createElement('li');
    li.classList.add('lista__item');
    li.innerText = texto[i];
    ul.appendChild(li);
}